#!/bin/bash

#exec &>> ~/logs/mysql_backup.log # Décommenter pour loguer le fonctionnement du script

exporter(){
	DATE=$(date '+%F' | sed 's/-//g')
	TIME=$(date '+%T' | sed 's/://g')
  DB_USER="db_username" #Utilisateur de la base de données : variable à modifier selon le besoin.
	DB_PASS="db_password" #Mot de passe de l'utilisateur de la base de données : variable à modifier selon le besoin.
	DB_HOST="localhost" #Machine hébergeant la base de données : variable à modifier selon le besoin. Dans votre cas "localhost" devrait être correct.
	DATABASES=$(MYSQL_PWD=$DB_PASS mysql -u $DB_USER -e "SHOW DATABASES;" | tr -d "| " | grep -v -e Database -e _schema -e mysql) #Sélection des BDD qui ne sont pas celles par défaut.

	for DB in $DATABASES #Les opérations suivantes seront effectuées pour chaque BDD sélectionnée.
	do
			backup
			outdir
			FULLPATH="$BACKUP/$OUTDIR"  #Chemin de sauvegarde : ajouter la destination souhaitée avant les variables, par exemple : "/home/user/$BACKUP/$OUTDIR"
			DB_NAME="${DB}_${DATE}_${TIME}.sql" #Les dumps seront sauvegardés au format example_20200819_162111.sql.
			MYSQL_PWD=$DB_PASS mysqldump -u $DB_USER --single-transaction --skip-lock-tables $DB -h $DB_HOST > $FULLPATH/$DB_NAME #Exportation du dump.
			archive
			remove
	done
}

backup(){
	BACKUP="mysql_backup" #Création d'un dossier pour les sauvegardes intitulé "mysql_backup": variable à modifier selon le besoin.
	if [[ ! -d $BACKUP ]]
	then
		mkdir $BACKUP #Créé un répertoire de sauvegarde portant le nom donné en variable.
	fi
}

outdir(){
	OUTDIR=$DB
	if [[ ! -d $BACKUP/$OUTDIR ]]
	then
		mkdir $BACKUP/$OUTDIR #Création d'un répertoire dont le nom est celui de la base de données sous le répertoire $BACKUP créé précédemment.
	fi
}

archive(){
	TAR_NAME="${DB_NAME}.tar.gz"
	tar zcvf $FULLPATH/$TAR_NAME -C $FULLPATH $DB_NAME && rm $FULLPATH/$DB_NAME #Compression du dump et suppresion de l'original.
}

remove(){
	COUNT="11" #Résultat de l'opération 19-8 correspondant aux heures de dump (de 8h à 19h).
	BASES=$(find $FULLPATH -name $DB\*.sql.tar.gz | sort -n)
	set $BASES

	if [[ $# -gt $COUNT ]]
	then
		rm $1 #Le 12e dump est supprimé.
	fi
}

exporter
