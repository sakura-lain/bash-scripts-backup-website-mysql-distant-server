#!/bin/bash

#exec &>> ~/logs/files_backup_all.log

main(){
SITENAME="site_name"

	timer
	for TIMEITEM in "${TIMER[@]}"
	do
		backup
		outdir
		timedir
		FULLPATH="$BACKUP/$OUTDIR/$TIMEDIR"
		archive
		send
		remove
	done
}

backup(){
	BACKUP="wordpress_backup"
	if [[ ! -d $BACKUP ]]
	then
		mkdir $BACKUP
	fi
}

outdir(){
	OUTDIR="${SITENAME}_public_html"
	if [[ ! -d $BACKUP/$OUTDIR ]]
	then
		mkdir $BACKUP/$OUTDIR
	fi
}

timer(){
	if [[ $(date "+%-H") -ge 0 ]] && [[ $(date "+%-H") -le 23 ]]
	then
		TIMER+=(hourly)
        fi
        if [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(daily)
	fi
        if [[ $(date "+%u") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(weekly)
	fi
        if [[ $(date "+%-d") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(monthly)
	fi
	if [[ $(date "+%-m") -eq 1 ]] || [[ $(date "+%-m") -eq 4 ]] || [[ $(date "+%-m") -eq 7 ]] || [[ $(date "+%-m") -eq 10 ]] && [[ $(date "+%-d") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(quarterly)
	fi
}

timedir(){
	TIMEDIR=$TIMEITEM
	if [[ ! -d $BACKUP/$OUTDIR/$TIMEDIR ]]
	then
		mkdir $BACKUP/$OUTDIR/$TIMEDIR
	fi
}

archive(){
	DATE=$(date '+%F' | sed 's/-//g')
	TIME=$(date '+%T' | sed 's/://g')
	TAR_NAME="${SITENAME}_public_html_${DATE}_${TIME}.tar.gz"
	#tar zcvf $FULLPATH/$TAR_NAME -C $FULLPATH ~/public_html && chmod 600 $TAR_NAME
	tar zcvf $FULLPATH/$TAR_NAME -C $FULLPATH ~/public_html
}
		
remove(){
	ARCHIVES=$(find $FULLPATH -name ${SITENAME}_public_html_\*.tar.gz | sort -n)
	
	set $ARCHIVES

	if [[ $# -gt 1 ]] 
	then 
		rm $1
	fi
}

send(){
	SSH_KEY="ssh_key"
	SSH_PORT="ssh_port"
	DISTSERV="distant_server"

	rsync -e "ssh -i $SSH_KEY -p $SSH_PORT" -avz --include="$OUTDIR/" --include="$TIMEDIR/" --exclude="$OUTDIR/*" --delete-after $BACKUP $DISTSERV
}

main
