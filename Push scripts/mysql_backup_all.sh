#!/bin/bash

#exec &>> ~/logs/mysql_backup_all.log

export(){
	DATE=$(date '+%F' | sed 's/-//g')
	TIME=$(date '+%T' | sed 's/://g')
    DB_USER="db_username"
	DB_PASS="db_password"
	DB_HOST="localhost"
	DATABASES=$(MYSQL_PWD=$DB_PASS mysql -u $DB_USER -e "SHOW DATABASES;" | tr -d "| " | grep -v -e Database -e _schema -e mysql)
	
	#set $DATABASES

	#for DB in $@
	for DB in $DATABASES
	do
		timer
		for TIMEITEM in "${TIMER[@]}"
		do
			backup
			outdir
			timedir
			FULLPATH="$BACKUP/$OUTDIR/$TIMEDIR"  ###
			DB_NAME="${DB}_${DATE}_${TIME}.sql"
			MYSQL_PWD=$DB_PASS mysqldump -u $DB_USER --single-transaction --skip-lock-tables $DB -h $DB_HOST > $FULLPATH/$DB_NAME
			archive
			send
			count
			remove
		done
	done
}

backup(){
	BACKUP="mysql_backup"
	if [[ ! -d $BACKUP ]]
	then
		mkdir $BACKUP
	fi
}

outdir(){
	OUTDIR=$DB
	if [[ ! -d $BACKUP/$OUTDIR ]]
	then
		mkdir $BACKUP/$OUTDIR
	fi
}

timer(){
        if [[ $(date "+%-H") -ge 0 ]] && [[ $(date "+%-H") -le 23 ]]
	then
		TIMER+=(hourly)
        fi
        if [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(daily)
	fi
        if [[ $(date "+%u") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(weekly)
	fi
        if [[ $(date "+%-d") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(monthly)
	fi
        if [[ $(date "+%-m") -eq 1 ]] || [[ $(date "+%-m") -eq 4 ]] || [[ $(date "+%-m") -eq 7 ]] || [[ $(date "+%-m") -eq 10 ]] && [[ $(date "+%-d") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(quarterly)
	fi
}

count(){
	case $TIMEITEM in
		"hourly")
			COUNT="24";;
		"daily")
			COUNT="7";;
		"weekly")
			COUNT="4";;
		"monthly")
			COUNT="3";;
	esac
}

timedir(){
	TIMEDIR=$TIMEITEM
	if [[ ! -d $BACKUP/$OUTDIR/$TIMEDIR ]]
	then
		mkdir $BACKUP/$OUTDIR/$TIMEDIR
	fi
}

archive(){
	TAR_NAME="${DB_NAME}.tar.gz"
	tar zcvf $FULLPATH/$TAR_NAME -C $FULLPATH $DB_NAME && rm $FULLPATH/$DB_NAME
}
		
remove(){
	BASES=$(find $FULLPATH -name $DB\*.sql.tar.gz | sort -n)
	
	set $BASES

	if [[ $TIMEITEM != "quarterly" ]]
	then
		if [[ $# -gt $COUNT ]] 
		then 
			rm $1
		fi
	fi
}

send(){
	SSH_KEY="ssh_key"
	SSH_PORT="ssh_port"
	DISTSERV="distant_server"

	rsync -e "ssh -i $SSH_KEY -p $SSH_PORT" -avz --include="$OUTDIR/" --include="$TIMEDIR/" --exclude="$OUTDIR/*" --delete-after $BACKUP $DISTSERV
}

export
