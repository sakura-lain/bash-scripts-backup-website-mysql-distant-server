#!/bin/bash

#exec &>> ~/logs/mysql_backup_daily.log

export(){
	DATE=$(date '+%F' | sed 's/-//g')
	TIME=$(date '+%T' | sed 's/://g')
    DB_USER="db_username"	
    DB_PASS="db_password"
	DB_HOST="localhost"
	DATABASES=$(MYSQL_PWD=$DB_PASS mysql -u $DB_USER -e "SHOW DATABASES;" | tr -d "| " | grep -v -e Database -e _schema -e mysql)
	
	set $DATABASES

	for DB in $@
	do
		backup
		outdir
		daily
		FULLPATH="$BACKUP/$OUTDIR/$DAILY"
		DB_NAME="${DB}_${DATE}_${TIME}.sql"
		MYSQL_PWD=$DB_PASS mysqldump -u $DB_USER --single-transaction --skip-lock-tables $DB -h $DB_HOST > $FULLPATH/$DB_NAME
		archive
		remove
		send
	done
}

remove(){
	BASES=$(find $FULLPATH -name $DB\*.sql.tar.gz | sort -n)
	
	set $BASES

	if [[ $# -gt 7 ]]
	then 
		rm $1
	fi
}

backup(){
	BACKUP="mysql_backup"
	if [[ ! -d $BACKUP ]]
	then
		mkdir $BACKUP
	fi
}

outdir(){
	OUTDIR=$DB
	if [[ ! -d $BACKUP/$OUTDIR ]]
	then
		mkdir $BACKUP/$OUTDIR
	fi
}

daily(){
	DAILY="daily"
	if [[ ! -d $BACKUP/$OUTDIR/$DAILY ]]
	then
		mkdir $BACKUP/$OUTDIR/$DAILY
	fi
}

archive(){
	TAR_NAME="${DB_NAME}.tar.gz"
	tar zcvf $FULLPATH/$TAR_NAME -C $FULLPATH $DB_NAME && rm $FULLPATH/$DB_NAME
}
		
send(){
	SSH_KEY="ssh_key"
	SSH_PORT="ssh_port"
	DISTSERV="distant_server"

	rsync -e "ssh -i $SSH_KEY -p $SSH_PORT" -avz --include="$OUTDIR/" --include="$TIMEDIR/" --exclude="$OUTDIR/*" --delete-after $BACKUP $DISTSERV
}

export
