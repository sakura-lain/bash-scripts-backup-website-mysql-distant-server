#!/bin/bash

#exec &>> ~/logs/files_backup_daily.log

SSH_KEY="ssh_key"
SSH_PORT="ssh_port"
DISTSERV="distant_server"

rsync -e "ssh -i $SSH_KEY -p $SSH_PORT" -avz --delete-after ~/public_html $DISTSERV
