#!/bin/bash
# A script to be run by root

#exec &>> /volume1/destination/logs/files_backup_pull_all_syno.log #Change the path to feet your needs

main(){
	SITENAME="site_name"
	OWNER="user:group" #The owner of the backups
	FTP_ADDRESS="example.com"
	FTP_CRED="user:password"
	PORT="21" #Change it if necessary

	DESTDIR="/volume1/destination" #Your destination directory on your Synology NAS
	cd $DESTDIR #For root user

	timer
	for TIMEITEM in "${TIMER[@]}"
	do
		backup
		outdir
		timedir
		FULLPATH="$BACKUP/$OUTDIR/$TIMEDIR"
		public_html
		#FTP="ftp://login:password@example.com/public_html/"
		#wget -m -np -b -c --directory-prefix=$FULLPATH $FTP
		#wget -m -np -b -c -r --directory-prefix=$FULLPATH ftp://login:password@example.com/public_html
		lftp ftp://${FTP_CRED}@${FTP_ADDRESS}:${PORT} -e "set ssl:check-hostname no; mirror -e /public_html $FULLPATH/public_html ; quit" #Assuming the files to retrieve are located in a /public_html directory itself located at the root of your server. lftp command cannot be put in a function (segmentation fault error)
		archive
		count
		remove
	done
}

backup(){
	BACKUP="wordpress_backups" #A directory for you Wordpress backups
	if [[ ! -d $BACKUP ]]
	then
		mkdir $BACKUP && chown $OWNER $BACKUP
	fi
}

outdir(){
	OUTDIR="${SITENAME}_public_html" #A directory dedicated to a specific website
	if [[ ! -d $BACKUP/$OUTDIR ]]
	then
		mkdir $BACKUP/$OUTDIR && chown $OWNER $BACKUP/$OUTDIR
	fi
}

timer(){
	if [[ $(date "+%-H") -ge 0 ]] && [[ $(date "+%-H") -le 23 ]]
	then
		TIMER+=(hourly)
  fi
  if [[ $(date "+%-H") -eq 0 ]]
	then
		TIMER+=(daily)
	fi
  if [[ $(date "+%u") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
  then
		TIMER+=(weekly)
	fi
  if [[ $(date "+%-d") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
  then
		TIMER+=(monthly)
	fi
	if [[ $(date "+%-m") -eq 1 ]] || [[ $(date "+%-m") -eq 4 ]] || [[ $(date "+%-m") -eq 7 ]] || [[ $(date "+%-m") -eq 10 ]] && [[ $(date "+%-d") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
  then
		TIMER+=(quarterly)
	fi
}

count(){
	case $TIMEITEM in
		"hourly")
			COUNT="24";;
		"daily")
			COUNT="7";;
		"weekly")
			COUNT="4";;
		"monthly")
			COUNT="3";;
	esac
}

timedir(){
	TIMEDIR=$TIMEITEM
	if [[ ! -d $BACKUP/$OUTDIR/$TIMEDIR ]]
	then
		mkdir $BACKUP/$OUTDIR/$TIMEDIR && chown $OWNER $BACKUP/$OUTDIR/$TIMEDIR
  fi
}

public_html(){
	if [[ ! -d $FULLPATH/public_html ]]
	then
		mkdir $FULLPATH/public_html && chown $OWNER $FULLPATH/public_html
  fi
}

archive(){
	DATE=$(date '+%F' | sed 's/-//g')
	TIME=$(date '+%T' | sed 's/://g')
	TAR_PREFIX="${SITENAME}_public_html" #Change it to suit your website name
	TAR_NAME="${TAR_PREFIX}_${DATE}_${TIME}.tar.gz"
	#tar zcvf $FULLPATH/$TAR_NAME -C $FULLPATH ~/public_html && chmod 600 $TAR_NAME
	tar zcvf $FULLPATH/$TAR_NAME -C $FULLPATH public_html/ && chown $OWNER $FULLPATH/$TAR_NAME && rm -r $BACKUP/$OUTDIR/$TIMEDIR/public_html
}

remove(){
	ARCHIVES=$(find $FULLPATH -name $TAR_PREFIX\*.tar.gz | sort -n)

	set $ARCHIVES
	if [[ $TIMEITEM != "quarterly" ]]
	then
		if [[ $# -gt $COUNT ]]
		then
			rm $1
		fi
	fi
}

main
