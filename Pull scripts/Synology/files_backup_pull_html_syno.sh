#!/bin/bash

#exec &>> /volume1/destination/logs/backup_pull_html_syno.log

main(){
	OWNER="user:group" #The owner of the backups
	SITENAME="site_name"
	SSH_ID="/path/to/ssh_key"
	SSH_LOGIN="user@server"
	REMOTE_DIR="/var/www/html"

###### For Synology root user only ######
	DESTDIR="/volume1/destination/"
	cd $DESTDIR
#########################################

	timer
	for TIMEITEM in "${TIMER[@]}"
	do
		backup
		outdir
		timedir
		FULLPATH="$BACKUP/$OUTDIR/$TIMEDIR"
		public_html
		rsync -e "ssh -i $SSH_ID" -avz --exclude="some_file_to_exclude" --exclude="some_dir_to_exclude" --exclude="*.log" $SSH_LOGIN:$REMOTE_DIR $FULLPATH/public_html
		#rsync -e "ssh -i $SSH_ID" -avz --exclude="some_file_to_exclude" --exclude="some_dir_to_exclude" --exclude="*.log" --delete $SSH_LOGIN:$REMOTE_DIR $FULLPATH/public_html
		chown -R $OWNER $FULLPATH
		#archive
		#count
		#remove
	done
}

backup(){
	BACKUP="wordpress_backups" #A directory for you Wordpress backups
	if [[ ! -d $BACKUP ]]
	then
		mkdir $BACKUP && chown $OWNER $BACKUP
	fi
}

outdir(){
	OUTDIR="${SITENAME}_public_html" #A directory dedicated to a specific website
	if [[ ! -d $BACKUP/$OUTDIR ]]
	then
		mkdir $BACKUP/$OUTDIR && chown $OWNER $BACKUP/$OUTDIR
	fi
}

timer(){
	if [[ $(date "+%-H") -ge 0 ]] && [[ $(date "+%-H") -le 23 ]]
	then
		TIMER+=(hourly)
  fi
  if [[ $(date "+%-H") -eq 0 ]]
	then
		TIMER+=(daily)
	fi
  if [[ $(date "+%u") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
  then
		TIMER+=(weekly)
	fi
  if [[ $(date "+%-d") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
  then
		TIMER+=(monthly)
	fi
#	if [[ $(date "+%-m") -eq 1 ]] || [[ $(date "+%-m") -eq 4 ]] || [[ $(date "+%-m") -eq 7 ]] || [[ $(date "+%-m") -eq 10 ]] && [[ $(date "+%-d") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
#  then
#		TIMER+=(quarterly)
#	fi
}

count(){
	case $TIMEITEM in
		"hourly")
			COUNT="4";;
		"daily")
			COUNT="7";;
		"weekly")
			COUNT="4";;
		"monthly")
			COUNT="3";;
	esac
}

timedir(){
	TIMEDIR=$TIMEITEM
	if [[ ! -d $BACKUP/$OUTDIR/$TIMEDIR ]]
	then
		mkdir $BACKUP/$OUTDIR/$TIMEDIR && chown $OWNER $BACKUP/$OUTDIR/$TIMEDIR
  fi
}

public_html(){
	if [[ ! -d $FULLPATH/public_html ]]
	then
		mkdir $FULLPATH/public_html && chown $OWNER $FULLPATH/public_html
  fi
}

archive(){
	DATE=$(date '+%F' | sed 's/-//g')
	TIME=$(date '+%T' | sed 's/://g')
	TAR_PREFIX="${SITENAME}_public_html" #Change it to suit your website name
	TAR_NAME="${TAR_PREFIX}_${DATE}_${TIME}.tar.gz"
	#tar zcvf $FULLPATH/$TAR_NAME -C $FULLPATH ~/public_html && chmod 600 $TAR_NAME
	tar zcvf $FULLPATH/$TAR_NAME -C $FULLPATH public_html/ && chown $OWNER $FULLPATH/$TAR_NAME && rm -r $BACKUP/$OUTDIR/$TIMEDIR/public_html
}

remove(){
	ARCHIVES=$(find $FULLPATH -name $TAR_PREFIX\*.tar.gz | sort -n)

	set $ARCHIVES
	if [[ $TIMEITEM != "quarterly" ]]
	then
		if [[ $# -gt $COUNT ]]
		then
			rm $1
		fi
	fi
}

main
