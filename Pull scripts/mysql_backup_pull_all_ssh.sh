#!/bin/bash

#exec &>> logs/mysql_backup_pull_all_ssh.log

main(){
	DATE=$(date '+%F' | sed 's/-//g')
	TIME=$(date '+%T' | sed 's/://g')
	SITENAME="site_name"
	OWNER="user:group" #The backups owner
	DB_PASS="dbpassword"
	DB_USER="dbuser"
	SSH_ID="/path/to/sshkey"
	SSH_LOGIN="user@server"
	DATABASES=$(ssh $SSH_LOGIN -i $SSH_ID "MYSQL_PWD=$DB_PASS mysql -u $DB_USER -P 3306 -e 'SHOW DATABASES;' | tr -d '| ' | grep -v -e Database -e _schema -e mysql") #On utilise la commande mysql du remote
#	DATABASES="db1 db2"

	for DB in $DATABASES
	do
		timer
		for TIMEITEM in "${TIMER[@]}"
		do
			backup
			outdir
			timedir
			dump
			archive
			count
			remove
		done
	done
}

backup(){
	BACKUP="mysql_backup"
	if [[ ! -d $BACKUP ]]
	then
		mkdir $BACKUP && chown $OWNER $BACKUP
	fi
}

outdir(){
	OUTDIR=${SITENAME}_${DB}
	if [[ ! -d $BACKUP/$OUTDIR ]]
	then
		mkdir $BACKUP/$OUTDIR && chown $OWNER $BACKUP/$OUTDIR
	fi
}

timer(){
        if [[ $(date "+%-H") -ge 0 ]] && [[ $(date "+%-H") -le 23 ]]
	then
		TIMER+=(hourly)
        fi
        if [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(daily)
	fi
        if [[ $(date "+%u") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(weekly)
	fi
        if [[ $(date "+%-d") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(monthly)
	fi
        if [[ $(date "+%-m") -eq 1 ]] || [[ $(date "+%-m") -eq 4 ]] || [[ $(date "+%-m") -eq 7 ]] || [[ $(date "+%-m") -eq 10 ]] && [[ $(date "+%-d") -eq 1 ]] && [[ $(date "+%-H") -eq 0 ]]
        then
		TIMER+=(quarterly)
	fi
}

count(){
	case $TIMEITEM in
		"hourly")
			COUNT="24";;
		"daily")
			COUNT="7";;
		"weekly")
			COUNT="4";;
		"monthly")
			COUNT="3";;
	esac
}

timedir(){
	TIMEDIR=$TIMEITEM
	if [[ ! -d $BACKUP/$OUTDIR/$TIMEDIR ]]
	then
		mkdir $BACKUP/$OUTDIR/$TIMEDIR && chown $OWNER $BACKUP/$OUTDIR/$TIMEDIR
	fi
}

dump(){
	FULLPATH="$BACKUP/$OUTDIR/$TIMEDIR"  ###
	DB_NAME="${SITENAME}_${DB}_${DATE}_${TIME}.sql"
	ssh -i $SSH_ID $SSH_LOGIN MYSQL_PWD=$DB_PASS mysqldump -u $DB_USER --single-transaction --skip-lock-tables $DB > $FULLPATH/$DB_NAME #On utilise la commande mysql du remote
}

archive(){
	TARNAME="${DB_NAME}.tar.gz"
#	tar zcvf $FULLPATH/$TAR_NAME -C $FULLPATH $DB_NAME && rm $FULLPATH/$DB_NAME
	tar zcvf $FULLPATH/$TARNAME -C $FULLPATH $DB_NAME && chown $OWNER $FULLPATH/$TARNAME && rm $FULLPATH/$DB_NAME
}

remove(){
	BASES=$(find $FULLPATH -name ${SITENAME}_$DB\*.sql.tar.gz | sort -n)

	set $BASES

	if [[ $TIMEITEM != "quarterly" ]]
	then
		if [[ $# -gt $COUNT ]]
		then
			rm $1
		fi
	fi
}

main
