# Bash scripts to backup a website and its MySQL database on a distant server

These scripts aim at backing up a website on a distant server, including its files and its MySQL database, using `rsync` via SSH with a SSH key authentication.

It is more secure to dump the MySQL database with a read-only user, in addition to entering the MySQL password as a variable.

The scripts can be launched via an hourly cron. They must be located in the directory in which you wish to save your data.

## Scripts

Two scripts are set to launch hourly, daily, weekly, monthly and quarterly backups in gzip format then send them to a distant server. They create folders to contain backups, one per period of time:

- `files_backup_all.sh`: backs up the website files. Keeps one backup per period on the website server, two on the distant server.
- `mysql_backup_all.sh`: backs up the website MySQL database. Keeps 24 hourly backups, 7 daily backups etc. on the website server, one more for each period of time on the distant server. Keeps all the quarterly backups.

Five scripts do the same thing, but instead of sending the backups from the remote to the backups host, they pull them from the remote to the backups host, which is more secure. They can also be used to dump a local database. Amongst them, two are configured to be launched from a Synology NAS (by root with permissions `700` and ownership `root:root`) and one to use SSH:

- `files_backup_pull_all.sh`
- `files_backup_pull_all_syno.sh`
- `mysql_backup_pull_all.sh`
- `mysql_backup_pull_all_syno.sh`
- `mysql_backup_pull_all_ssh.sh`

Two scripts are programmed to make daily backups only :

- `files_backup_daily.sh`: it's just a simple rsync command
- `mysql_backup_daily.sh`: backs up the website MySQL database in gzip format. Keeps seven backups (one more on the distant server). To be adapted to fit other periods of time.

The last script, `mysql_backup.sh`, does the same as the second one excepts that it only keeps 11 tar archives (one per working hour between 8am and 2 pm) and doesn't send them to a distant server.

### Instructions:

- Uncommenting the first line will generate a log file on the website server, assuming you want to locate it the `logs` folder.
- The scripts also assume that your files are located in a `public_html` folder on your website server, please change it too if it doesn't suit you.

## Variables:

### General:
- `BACKUP`: Name of your backup directory.

### For MySQL:
- `DB_USER`: the MySQL database username ;
- `DB_PASS`: password of the MySQL database user ;
- `DB_HOST`: MySQL server address, usually localhost, can be a remote server too ;
- `DB_PORT`: MySQL port, default is 3306.

### For the files backups:
- `SITENAME`: the name of the website, to be implemented in the backups files names.

### For rsync via SSH:
- `SSH_KEY`: location of the SSH key file on the website server ;
- `SSH_PORT`: SSH port of the distant server (22 as default) ;
- `DISTSERV`: distant server address, with the arborescence were the backups must be stored under the template `user@example.com:/home/backups`.

### For lftp:
- `FTP`: your FTP credentials, following the template `ftp://login:password@example.com` ;
- `PORT`: FTP port, default is 21.

### For pull scripts:
- `OWNER`: the username and the group to whom the backed up files and databases will belong on the server receiving the data; noted as `user:group`

### For Synology NAS:
- `DESTDIR`: The backups destination directory.
